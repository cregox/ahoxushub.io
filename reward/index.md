---
permalink: /reward/
layout: page
title: reward
published: true
---

So you've found one of my lost goodies, huh?

Then, please, [contact me](/contact) to arrange your reward!
