---
permalink: /tv/
layout: page
title: fas tv
published: true
fastv: https://plus.google.com/collection/4Qqrb
drive: https://drive.google.com/open?id=0B7x-Yn04UquwWEpGdFZPT0F5Nkk
---

at some point in 2015 I thought it would be fun to try making something like [9gag tv](http://9gag.com/tv) or [iflscience](http://www.iflscience.com/team/elise-andrew/) (later on I found out about something [similar](https://www.patreon.com/lolhehehe) to my will, except I have no wish to produce anything in non-english).

I ended up creating [a private category (you won't be able to see this link) in talk](https://talk.cregox.com/c/fas-tv) called "fas tv", to see how good (or bad) I could build it over some time, before publishing. had this written on its about:

> curated [f]un, [a]wesome and [s]cience items from the web. content that could show in an **adult** TV. only our very favourite ones.

well, it never worked.

I still **may** try to resurrect this project since I actually got [plenty related offline files]({{page.drive}}) I wished to share in a more interesting fashion (and probably not everything). also because (as evident from the collection below) links break. and it hurts. [me](/backup). :(

although it will probably stay dead (or undead).

meanwhile, I think there's a **good and fun** collection of links that came out of all this: [the google plus one]({{page.fastv}}). which I ocasionally still update. ps: the [one already mentioned in g drive]({{page.drive}}) isn't quite a "good" collection... it's just a random one. and so it's probably only fun(-ish) to me and perhaps a few other folks who may never get to see it.

tl;dr;

here's the one result from this (yet another) failed project:

# [cregox fas tv on g+]({{page.fastv}})

[![community pool](community pool.gif)]({{page.fastv}})

<small>_community series - almost as good as scrubs_</small>
