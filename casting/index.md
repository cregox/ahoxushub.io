---
permalink: /casting/
layout: page
title: casting
published: true
---

Brief greetings.

I'd love to get casted in any project which would let me do more than acting. Ideally I'd be getting classes in some cinema group like [ETA](http://www.estudiodetreinamento.com.br/). They basically teach theater, free of any regular fees (so, with affordable rates) and most of their income comes from student playing plays, right away from module 1. You learn how to get income as an artist. But I don't live there in Sao Paulo anymore, nor do I want to.

Despite being a fucking great actor (proportional to the amount of practice) and generally above average in anything I do (**perhaps** except sports) I'm also way too unexperienced in making scenes to get spotted as a professional there. Who knows? One day I could get the guts to go "live in the streets" (in a sense, you know, because the absolute majority of artists get no money) and enable myself to start working with some kind of art 24/7...

Until then, I won't have much to show - <small>unless you want to start **digging** me up online: boy, as a web freak I've created small "artistic" little pieces, such as [this old thing](https://en.wikipedia.org/wiki/User:Cregox/-%3F_wiki%3F) (which is now broken and I don't recall [what was it](https://en.wikipedia.org/w/index.php?title=User:Cregox&oldid=220362058) but it sure was a form of art!). For a terrible instance, [my mom](https://www.quora.com/What-kind-of-hobbies-do-highly-intelligent-i-e-those-with-intelligence-levels-beyond-the-exceptional-people-have/answer/Caue-Rego?srid=ptsW) surely loves every little thing I do but not so much of my other 3 younger brothers. I believe it has to do with "[the art](https://github.com/cauerego/cauerego.github.io/wiki/a-novel-about-the-other-novel)" which me, her and some friends can see. Hopefully soon [a basiux](http://basiux.org) will change all that... And be a mind babelfish! :)</small>

Anyway, below I'm trying to gather a collection of relevant links. Hope you can enjoy!

Photos

- [casting](https://b.cregox.com/caue-casting) is a more traditional (and boring) style, if you're into this
- probably I should use more [instagram](https://www.instagram.com/cregox/), haha
- [random fun](/random), [why not](http://talk.cregox.com/t/focus-on-mario-forget-the-rest-of-universe/7919)? randomness kinda says a lot about my current idea-project
- back to [myself](/myself) then, with more actual real photos

Somewhat relevant projects

- [Abaixa Tampa (Lower the toilet lid)](http://abaixatampa.wordpress.com/), a theater play in portuguese made in 3 months
- [Funny or Die with Jean Claude](http://www.funnyordie.com/videos/f6f674e14c/just-a-regular-damme-day), “cool” video made in about 5 days
- [Youtube Channel](https://www.youtube.com/c/CaueRego) one day I could become a youtuber, even while I'm still far from it

"Poetry" (or some sore of art in form of text)

- [talk.cregox.com](http://talk.cregox.com/) a ["ghost town"](http://talk.cregox.com/t/a-beginning-for-the-forums-here/7#7) I've made and home to most of my [blog](/blog) as an [inform**art**icist](http://talk.cregox.com/t/to-kstanley-can-a-neat-mario-start-up-a-basiux/7914) and [progammer*](http://talk.cregox.com/t/yeah-im-also-a-progammer/7676)
- [The next Ai poca lips](http://blog.cregox.com/the-next-ai-poca-lips-r23K6m6)
