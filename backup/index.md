---
permalink: /backup/
layout: page
title: backup manifesto
published: true
---

I've wrote too much about backup. a lot of times.

it always ends up being about how I was trying to [keep some of my data to myself](https://medium.cregox.com/how-to-keep-all-your-data-to-yourself-930ac30cae7d).

one of the big beauties of computers is how they turn copying data into an easy task.

make it simple to generate redundancy and avoid losing important data.

but what data is important?

trying to answer that is like trying to predict the future.

I want to keep all my data, or as much as possible (in special, textual and such small data points).

and can I trust myself to always do it manually?

true backup needs to be automatic, ffs. computers are also much more reliable.

problem is, even today, that's still a freaking difficult quest. lost lots of data on the way. :(

right now I'm using mainly a combination of 1 local backup and 2 different clouds. with basically no automation of videos and web content, which I still have to manually download to my main machine.

guess we'll have to _keep on trying_...

![walking on autumn leaves](walking on autumn leaves.gif)

<small>_[should I](/contact) be using less gif's?_</small>
