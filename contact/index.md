---
permalink: /contact/
layout: page
title: contact
published: true
menu: true
redirect_from: /contacts/
---

Aha, no form here!

You may:

- use the [forums](http://talk.cregox.com)
- mail [help@cregox.com](mailto:help@cregox.com)
- call me in Portugal at **+351 918424479**
- reach me on **skype** at _**caue.rego**_
- try calling me at google at **+1 732 7377346** (or leave a message there)

![ana boat](caue on ana boat.jpeg)
*I wish I lived in such a boat... Flying in the sky...*
