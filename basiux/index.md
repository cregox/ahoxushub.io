---
permalink: /basiux/
layout: page
title: benevolent artificial super intelligence user experience
published: true
---

project in hiatus almost since its [launch](http://htmlpreview.github.io/?https://github.com/basiux/basiux.github.io/blob/76498bfeee42d1d458c1554bb00615254337eeff/index.html), [days](https://github.com/basiux/basiux.github.io/tree/76498bfeee42d1d458c1554bb00615254337eeff) before [open.ai's](https://blog.openai.com/introducing-openai/) in Dec 2015 [#envy](https://github.com/cauerego/cauerego.github.io/wiki/a-novel-about-the-other-novel).

although [the website](http://basiux.org) may not make it clear, this is basically a named-one-man-idea that grew into [experiences](https://www.quora.com/What-is-the-most-advanced-artificial-intelligence-in-a-video-game-in-history), own learning through [research](https://plus.google.com/collection/8JTrh) and [meetings](https://www.meetup.com/basiux-lisbon/), and [nothing else](http://cregox.com/talk/t/about-the-basiux-category/7683.html).

[read more](/talk/t/what-is-the-basiux/7675.html) or [take a look](http://basiux.org).
